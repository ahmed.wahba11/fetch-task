let show = document.getElementById('show');

fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(data =>{
        for(let i = 0 ;i<data.length;i++) {
            show.innerHTML += `<li data-id=${data[i].id}>`+`${data[i].title}</li>`;

        }
        show.addEventListener('click',function (event) {
            let elem = event.target;
            if(elem.tagName === 'LI' ){
                let id = elem.dataset.id -1;

                elem.classList.add('completed');
                elem.innerHTML += `<p>${data[id].completed} For ID: ${data[id].id}}</p>`;
            }
        })

        console.table(data)
    });



